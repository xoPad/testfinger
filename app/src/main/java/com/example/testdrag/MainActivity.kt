package com.example.testdrag

import android.content.ClipData
import android.content.ClipDescription
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.util.Log
import android.view.DragEvent
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

//    private class MyDragShadowBuilder(v: View) : View.DragShadowBuilder(v) {
//
//        private val shadow = ColorDrawable(Color.LTGRAY)
//
//        override fun onProvideShadowMetrics(size: Point, touch: Point) {
//            val width: Int = view.width / 2
//            val height: Int = view.height / 2
//            shadow.setBounds(0, 0, width, height)
//            size.set(width, height)
//            touch.set(width / 2, height / 2)
//        }
//
//        override fun onDrawShadow(canvas: Canvas) {
//            shadow.draw(canvas)
//        }
//    }

    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        biometricLogin.setOnClickListener {
            biometricPrompt.authenticate(promptInfo)
        }


        qwerty.setOnClickListener {
            val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            if (netInfo != null && netInfo.isConnectedOrConnecting) {
                Toast.makeText(this, "Yes", Toast.LENGTH_LONG).show()
            } else Toast.makeText(this, "No", Toast.LENGTH_LONG).show()

        }
//http://corochann.com/android-tv-application-hands-on-tutorial-2-85.html
        //https://m.habr.com/ru/post/316260/
//        qwerty.setOnLongClickListener { v: View ->
//            val item = ClipData.Item(v.tag as? CharSequence)
//
//            val dragData = ClipData(
//                v.tag as? CharSequence,
//                arrayOf(ClipDescription.MIMETYPE_TEXT_PLAIN),
//                item
//            )
//
//            val myShadow = MyDragShadowBuilder(qwerty)
//
//            // Starts the drag
//            v.startDrag(
//                dragData,   // the data to be dragged
//                myShadow,   // the drag shadow builder
//                null,       // no need to use local data
//                0           // flags (not currently used, set to 0)
//            )
//
//        }
        val executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    Toast.makeText(
                        applicationContext,
                        "Authentication error: $errString", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    Toast.makeText(
                        applicationContext,
                        "Authentication succeeded!", Toast.LENGTH_SHORT
                    )
                        .show()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(
                        applicationContext, "Authentication failed",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometric login for my app")
            .setSubtitle("Log in using your biometric credential")
            .setNegativeButtonText("Use account password")
            .build()

    }
}


//    private final class MyTouch: View.OnTouchListener{
//
//        var xDelta:Float? = null
//        var yDelta:Float? = null
//
//        override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
//
//            val x = p1!!.rawX
//            val y = p1.rawY
//
//            when(p1.action){
//                MotionEvent.ACTION_DOWN->{
//                    val params = p0!!.layoutParams as RelativeLayout.LayoutParams
//                    xDelta = x - params.leftMargin
//                    yDelta = y - params.topMargin
//                }
//                MotionEvent.ACTION_UP->{}
//                MotionEvent.ACTION_POINTER_UP->{}
//                MotionEvent.ACTION_POINTER_DOWN->{}
//                MotionEvent.ACTION_MOVE->{
//                    val params = p0!!.layoutParams as RelativeLayout.LayoutParams
//                    params.leftMargin = (x - xDelta!!).roundToInt()
//                    params.topMargin = (y - yDelta!!).roundToInt()
//                    params.rightMargin = -250
//                    params.bottomMargin = -250
//                    p0.layoutParams = params
//                }
//            }
//            return true
//        }
//    }

